=== Newswire Integration ===
Contributors: Newswire
Donate link: https://
Tags: blockchain, crypto, cryptocurrency, news, broadcast 
Requires at least: 4.6
Tested up to: 6.1.1
Stable tag: 1.0.15
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin allows to import the latest blockchain news into your website.

== Description ==

Newswire is your one-stop broadcast shop for the blockchain news. Distribute your press releases automatically to the leading publications with guaranteed coverage and detailed analytics.

This plugin allows you a direct integration with our platform.

A few notes about this plugin:

*   After installation please go under "Settings" >> "Newswire"
*   On this page please provide your Secret and Token (you can get it by contacting the manager of this plugin)
*   You can also set default "Post Status" and "Categories" which be connected with articles published with Newswire
*   Save changes

== Installation ==

= From WordPress =
1. Install
2. Activate
3. Enter your token and secret key.

= From WordPress backend =

1. Navigate to Plugins -> Add new.
2. Click the button "Upload Plugin" next to "Add plugins" title.
3. Upload the downloaded zip file and activate it.
4. Enter your token and secret key.

= Direct upload =

1. Upload the downloaded zip file into your `wp-content/plugins/` folder.
2. Unzip the uploaded zip file.
3. Navigate to Plugins menu on your WordPress admin area.
4. Activate this plugin.
5. Enter your token and secret key.

== Frequently Asked Questions ==

= How can I generate a token and secret? =
Please contact the manager of this plugin - blockchain news publisher"

== Screenshots ==



== Changelog ==

= 1.0.0 (20th Nov ,2023)=
* Plugin created

== Upgrade Notice ==
= Upgrade your old version to 1.0.0