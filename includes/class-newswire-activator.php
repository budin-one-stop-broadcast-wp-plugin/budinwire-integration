<?php

/**
 * Fired during plugin activation
 *
 * @link       https://
 * @since      1.0.0
 *
 * @package    Newswire
 * @subpackage Newswire/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Newswire
 * @subpackage Newswire/includes
 * @author     Minor, Newswire
 */
class NewswireActivator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {

    }

}
