<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://
 * @since      1.0.0
 *
 * @package    Newswire
 * @subpackage Newswire/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Newswire
 * @subpackage Newswire/includes
 * @author     Minor, Newswire
 */
class NewswireDeactivator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function deactivate()
    {

    }

}
