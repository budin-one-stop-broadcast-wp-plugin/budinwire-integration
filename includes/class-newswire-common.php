<?php

const newswire = 'newswire';
const financewire = 'financewire';
const cyberwire = 'cyberwire';
const techwire = 'techwire';

const domain_newswire = 'https://';
const domain_cyberwire = 'https://';
const domain_financewire = 'https://';

const author_username_newswire = 'newswire';
const author_email_newswire = 'contact@newswire.org';
const author_first_name_newswire = 'Newswire';
const author_url_newswire = 'https:///';
const author_avatar_newswire = '/wp-plugin/author_avatar_newswire.png';

const author_username_cyberwire = 'cyberwire';
const author_email_cyberwire = 'contact@cyber-wire.org';
const author_first_name_cyberwire = 'Cyberwire';
const author_url_cyberwire = 'https://cyber-wire.org';
const author_avatar_cyberwire = '/wp-plugin/author_avatar_cyberwire.png';

const author_username_financewire = 'financewire';
const author_email_financewire = 'contact@financewire.com';
const author_first_name_financewire = 'FinanceWire';
const author_url_financewire = 'https://financewire.com';
const author_avatar_financewire = '/wp-plugin/author_avatar_financewire.png';

const author_last_name = null;
const author_twitter = null;
const author_facebook = null;
const author_google = null;
const author_tumblr = null;
const author_instagram = null;
const author_pinterest = null;

class NewswireCommon
{
    protected $wires = [];
    protected $plugin_id;

    public function __construct($plugin_id)
    {
        $this->plugin_id = $plugin_id;
        $this->wires = [newswire, financewire, cyberwire, techwire];
    }

    public function get_wires()
    {
        return $this->wires;
    }

    /**
     * @param $wire
     * @return object
     */
    public function get_credentials_for_wire($wire = null)
    {
        switch ($wire) {
            case financewire:
                $username = author_username_financewire;
                $email = author_email_financewire;
                $avatar = author_avatar_financewire;
                $domain = domain_financewire;
                break;
            case cyberwire:
                $username = author_username_cyberwire;
                $email = author_email_cyberwire;
                $avatar = author_avatar_cyberwire;
                $domain = domain_cyberwire;
                break;
            default:
                $wire = newswire;
                $username = author_username_newswire;
                $email = author_email_newswire;
                $avatar = author_avatar_newswire;
                $domain = domain_newswire;
                break;
        }
        return (object)[
                'wire' => $wire,
                'username' => $username,
                'email' => $email,
                'avatar' => $avatar,
                'domain' => $domain
        ];
    }

    /**
     * @param $wire
     * @return array
     */
    public function get_user_data($wire)
    {
        switch ($wire) {
            case cyberwire:
                $first_name = author_first_name_cyberwire;
                $user_url = author_url_cyberwire;
                break;
            case financewire:
                $first_name = author_first_name_financewire;
                $user_url = author_url_financewire;
                break;
            default:
                $first_name = author_first_name_newswire;
                $user_url = author_url_newswire;
                break;
        }
        return [
                'first_name' => $first_name,
                'user_url' => $user_url,
                'last_name' => author_last_name,
                'twitter' => author_twitter,
                'facebook' => author_facebook,
                'google' => author_google,
                'tumblr' => author_tumblr,
                'instagram' => author_instagram,
                'pinterest' => author_pinterest,
        ];
    }

    public function get_admin_category_options($categories)
    {
        $category_options = '';
        foreach ($categories as $c) {
            $category_options .= '<option value="' . $c->name . '">' . $c->name . '</option>';
        }
        return $category_options;
    }

    public function get_admin_wire_options($options = [])
    {
        $wires_options = [];
        $plugin_id = $this->plugin_id;

        $label_category = translate('Main Category', $this->plugin_id);
        $label_additional_categories = translate('Additional Categories', $this->plugin_id);

        foreach ($this->wires as $w) {
            $label = null;
            $link = null;
            $desc = null;
            $option_name_category = 'category';
            $option_name_additional_categories = 'additional_categories';
            switch ($w) {
                case newswire:
                    $label = 'Newswire';
                    $link = 'https://';
                    $desc = 'Newswire is the leading blockchain and crypto newswire and press release distribution service that maximize crypto news coverage.';
                    break;
                case financewire:
                    $label = 'Financewire';
                    $link = 'https://app.financewire.com';
                    $desc = 'Broadcast your news to industry-leading financial media outlets with guaranteed and immediate exposure.';
                    break;
                case cyberwire:
                    $label = 'Cyberwire';
                    $link = 'https://app.cyber-wire.org';
                    $desc = 'Cyberwire is the leading cyber newswire and press release distribution service that maximize cyber-security news coverage.';
                    break;
                case techwire:
                    $label = 'Techwire';
                    $desc = 'Broadcast your news to industry-leading technology media outlets with guaranteed and immediate exposure.';
                    break;
            }

            $option_name_category = $this->get_wire_field($w, $option_name_category);
            $option_name_additional_categories = $this->get_wire_field($w, $option_name_additional_categories);

            $category = $this->get_plugin_internal_option($options, $option_name_category);
            $additional_categories = $this->get_plugin_internal_option($options, $option_name_additional_categories, '');

            $config = [
                    'wire' => $w,
                    'description' => $desc,
                    'link' => $link,
                    'label' => $label,
                    'option_category' => [
                            'field_name' => $option_name_category,
                            'label' => $label_category,
                            'id' => $plugin_id . '-' . $option_name_category,
                            'name' => $plugin_id . '[' . $option_name_category . ']',
                            'value' => $category,
                    ],
                    'option_additional_categories' => [
                            'field_name' => $option_name_additional_categories,
                            'label' => $label_additional_categories,
                            'id' => $plugin_id . '-' . $option_name_additional_categories,
                            'name' => $plugin_id . '[' . $option_name_additional_categories . ']',
                            'value' => !empty($additional_categories) ? $additional_categories : "",
                    ]
            ];
            $wires_options[] = $config;
        }
        return $wires_options;
    }

    public function get_wire_field($wire, $field)
    {
        if ($wire !== newswire) {
            return $wire . '_' . $field;
        }
        return $field;
    }

    public function get_plugin_internal_option($options, $name, $default_value = null)
    {
        $option_value = isset($options[$name]) ? $options[$name] : null;
        return sanitize_text_field($option_value !== null ? $option_value : $default_value);
    }

}
