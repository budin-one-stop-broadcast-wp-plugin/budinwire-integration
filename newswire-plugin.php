<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://
 * @since             1.0.0
 * @package           Newswire
 *
 * @wordpress-plugin
 * Plugin Name:       Newswire Integration
 * Plugin URI:        https://
 * Description:       Integrate Your Blog With blockchain news *** Platform
 * Version:           1.0.0
 * Author:            Minor
 * Author URI:        https://
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       newswire
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0
 * Rename this for your plugin and update it as you release new versions.
 */
define('NEWSWIRE_INTEGRATION_PLUGIN', '1.0.0');

/**
 * The code that runs during plugin activation.
 */
function activate_newswire_plugin()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-newswire-activator.php';
    NewswireActivator::activate();
}

/**
 * The code that runs during plugin deactivation.
 */
function deactivate_newswire_plugin()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-newswire-deactivator.php';
    NewswireDeactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_newswire_plugin');
register_deactivation_hook(__FILE__, 'deactivate_newswire_plugin');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-newswire.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_newswire_plugin()
{

    $plugin = new Newswire();
    $plugin->run();

}

run_newswire_plugin();